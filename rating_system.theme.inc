<?php

/**
 * @file
 * Provides the theming functions for rating system
 */

/**
 * Show a fivestar.
 */
function theme_rating_system_fivestar($variables) {
  $output = '<div class="rating-system-fivestar-wrapper">';
  $output .= '<div class="rating-system-fivestar-inner">★★★★★';
  $output .= '<div class="rating-system-fivestar-active" style="width: ' . $variables['percentage'] . '%;">★★★★★';
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}
