<?php
/**
 * @file
 * All token related functions and hooks.
 */

/**
 * Implements hook_token_info().
 */
function rating_system_token_info() {
  $info = array();

  $rating_formulas = rating_formula_load();
  if ($rating_formulas) {
    foreach ($rating_formulas as $rating_formula) {
      $info['tokens'][$rating_formula->entity_type]['rating_system_' . $rating_formula->name . '_score'] = array(
        'name' => $rating_formula->label . ' ' . t('score'),
        'description' => $rating_formula->label . ' ' . t('score'),
      );

      $info['tokens'][$rating_formula->entity_type]['rating_system_' . $rating_formula->name . '_percentage'] = array(
        'name' => $rating_formula->label . ' ' . t('percentage'),
        'description' => $rating_formula->label . ' ' . t('percentage'),
      );
    }
  }

  // Return them.
  return $info;
}

/**
 * Implements hook_tokens($type, $tokens, array $data = array(), array $options = array().
 */
function rating_system_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  $rating_formulas = rating_formula_load();
  if ($rating_formulas) {
    foreach ($rating_formulas as $rating_formula) {
      if ($type == $rating_formula->entity_type) {

        foreach ($tokens as $name => $original) {
          if ($name == 'rating_system_' . $rating_formula->name . '_score') {
            $replacements[$original] = rating_system_get_entity_formula_score($data[$type], $rating_formula);
          }

          if ($name == 'rating_system_' . $rating_formula->name . '_percentage') {
            $score = rating_system_get_entity_formula_score($data[$type], $rating_formula);
            $replacements[$original] = rating_system_get_entity_formula_percentage_by_score($score, $rating_formula);
          }
        }
      }
    }
  }

  return $replacements;
}
