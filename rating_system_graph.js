/**
 * @file
 * Create the charts with the rating.
 */

(function ($) {
  Drupal.behaviors.ratingSystemChart = {
    attach: function (context, settings) {

      if (context == document) {
        new Morris.Line({
          // ID of the element in which to draw the chart.
          element: 'morris-graph',
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: settings.rating_system.graphData,
          // The name of the data record attribute that contains x-values.
          xkey: 'timestamp',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['max', 'score', 'min'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['max', 'score', 'min'],

          // colors
          lineColors: ['#cccccc', '#333333', '#cccccc']
        });
      }

    }
  };

})(jQuery);
