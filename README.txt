#Rating system documentation

##Purpose
The rating system makes it possible to attach formules to drupal entities.
These formulas can contain tokens. So the possibilities are endless.

Articles could have ratings,
by each fivestar rating on the comments,
just with one formula and the right tokens.

To extend that idea more you could add the token with the article
page views of the last month, and you will have a dynamic rating.

##Intergrations
Views
Fields (a fivestar display can be attached to entities)
Features

##Requirements:
libraries
token
entity

You can use Entity token multiple field logic for complex math logic tokens.
This module is written for use with rating system.
https://drupal.org/sandbox/danielbeeke/2097423

Morris, a charting library.
https://github.com/oesmith/morris.js/archive/0.4.3.zip

Extract contents to sites/all/libraries/morris

Raphael, a svg to canvas libary.
https://github.com/DmitryBaranovskiy/raphael/raw/master/raphael-min.js

Save it to sites/all/libraries/raphael/raphael-min.js
