<?php
/**
 * @file
 * Main rating system function.
 * Returns a score for an entity.
 * The function calling this one is responsible for using revisions when needed.
 */

/**
 * Returns a score for an entity.
 */
function rating_system_get_entity_formula_score($entity, $rating_formula, $reset = FALSE) {
  if (!rating_system_check_rpn()) {
    return;
  }

  $entity_info = entity_get_info($rating_formula->entity_type);

  $entity_id = $entity->{$entity_info['entity keys']['id']};

  $rating_system_cached_scores = &drupal_static(__FUNCTION__);
  if (!isset($rating_system_cached_scores[$entity_id]) || $reset) {
    $cached = rating_system_get_cached_entity_formula_score($entity_id, $rating_formula);

    if (!empty($cached) && !$reset) {
      $rating_system_cached_scores[$rating_formula->rfid][$entity_id] = $cached;
    }
    else {
      $formula = $rating_formula->calculation;

      // We check this static inside rating_system_entity_load so there will be no recursive shit.
      $rating_system_current_entities = &drupal_static('rating_system_current_entities');
      $rating_system_current_entities[$rating_formula->name][$entity_id] = $entity_id;

      // Replace the tokens with real values.
      $formula = token_replace($formula, array(
        $rating_formula->entity_type => $entity
      ));

      include_once(libraries_get_path('pear-rpn') . '/RPN.php');

      $rpn = new Math_Rpn();

      // Try to do some math
      // This PEAR class has a deprecated warning therefore we use @.
      // Add 0 + so we can use one token and no calculation.
      $score = @$rpn->calculate('0 + ' . $formula, 'deg', FALSE);

      $rating_system_cached_scores[$rating_formula->rfid][$entity_id] = $score;

      rating_system_set_cached_entity_formula_score($entity_id, $rating_formula, $score);
    }
  }

  return $rating_system_cached_scores[$rating_formula->rfid][$entity_id];
}

/**
 * Returns a cached score for an entity.
 */
function rating_system_get_cached_entity_formula_score($entity_id, $rating_formula) {
  $score = db_select('rating_score', 's')
  ->fields('s', array('score'))
  ->condition('rfid', $rating_formula->rfid, '=')
  ->condition('entity_id', $entity_id, '=')
  ->condition('entity_type', $rating_formula->entity_type, '=')
  ->execute()
  ->fetchField();

  return $score;
}

/**
 * Set a cached score for an entity.
 */
function rating_system_set_cached_entity_formula_score($entity_id, $rating_formula, $score) {
  $record = array(
    'rfid' => $rating_formula->rfid,
    'entity_type' => $rating_formula->entity_type,
    'entity_id' => $entity_id,
    'score' => $score,
    'timestamp' => REQUEST_TIME
  );

  // Update the old record.
  if (rating_system_get_cached_entity_formula_score($entity_id, $rating_formula)) {
    drupal_write_record('rating_score', $record, array('rfid', 'entity_type', 'entity_id'));
  }

  // Or insert a new one.
  else {
    drupal_write_record('rating_score', $record);
  }

  rating_system_write_entity_formula_score_revision($entity_id, $rating_formula, $score);

  return $score;
}

/**
 * Flushes a cached score for an entity.
 */
function rating_system_flush_cached_entity_formula_score($entity_id, $rating_formula) {
  $rows_deleted = db_delete('rating_score')
  ->condition('rfid', $rating_formula->rfid, '=')
  ->condition('entity_id', $entity_id, '=')
  ->condition('entity_type', $rating_formula->entity_type, '=')
  ->execute();

  return $rows_deleted;
}

/**
 * Writes the revision record to the database.
 */
function rating_system_write_entity_formula_score_revision($entity_id, $rating_formula, $score) {
  $old_score = rating_system_entity_get_score_last_revision($entity_id, $rating_formula);

  // We don't need to write to much to the score table.
  if ($old_score != $score && $old_score['timestamp'] != REQUEST_TIME) {
    $record = array(
      'rfvid' => $rating_formula->rfid,
      'entity_type' => $rating_formula->entity_type,
      'entity_id' => $entity_id,
      'score' => $score,
      'timestamp' => REQUEST_TIME
    );

    drupal_write_record('rating_score_revision', $record);

    $min_max = rating_system_get_formula_min_max_by_other_scores($rating_formula);

    $last_min_max_written = rating_system_get_formula_min_max($rating_formula);

    if ($last_min_max_written['timestamp'] != REQUEST_TIME) {
      $record = array(
        'rfvid' => $rating_formula->rfid,
        'min' => $min_max['min'],
        'max' => $min_max['max'],
        'timestamp' => REQUEST_TIME
      );

      drupal_write_record('rating_score_min_max', $record);
    }
  }
}

/**
 * Get the last revision of a score.
 */
function rating_system_entity_get_score_last_revision($entity_id, $rating_formula) {
  $score = db_select('rating_score_revision', 's')
  ->fields('s', array('score', 'timestamp'))
  ->condition('rfvid', $rating_formula->vid, '=')
  ->condition('entity_id', $entity_id, '=')
  ->condition('entity_type', $rating_formula->entity_type, '=')
  ->orderBy('timestamp', 'DESC')
  ->range(0, 1)
  ->execute()
  ->fetchAssoc();

  if ($score) {
    return $score;
  }
}

/**
 * Returns a cached score for an entity.
 */
function rating_system_get_formula_min_max($rating_formula) {
  $min_max = db_select('rating_score_min_max', 's')
  ->fields('s')
  ->condition('rfvid', $rating_formula->rfid, '=')
  ->orderBy('timestamp', 'DESC')
  ->range(0, 1)
  ->execute()
  ->fetchAssoc();

  return $min_max;
}
